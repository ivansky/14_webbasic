# AutoShop Tomcat WebServer
## Task Description
Using servlets and JSP and results of 'JDBC', 'HTML, CSS basic' homeworks implement the following:

### First page: 
- login form with link to registration

### Second page:
- car list (retrieved from DB)
- available only to logged in users (direct access via URL need to be restricted)
- at the top of page username (not login) of authenticated user should be shown
- logout link which invalidates session and redirects to login page

### Registration page: 
allows to create a new user in DB (login, username, matching passwords). In case of successful registration - redirects to car list page. Prevent registration of already registered login with meaningful message.

**Returning content type:** text/html with UTF-8 (set it explicitly in response)
Deploy/run app to tomcat server (http://tomcat.apache.org/) under "/store" context path using maven plugin
Use annotations to configure servlets/filters.
Scriptlets are forbidden. Use JSTL tags.

## Optional
Use secure password storage with hash and salt: http://arr.gr/blog/2012/01/storing-passwords-the-right-way/
AJAX check of user existence during registration.
Thymeleaf as template engine